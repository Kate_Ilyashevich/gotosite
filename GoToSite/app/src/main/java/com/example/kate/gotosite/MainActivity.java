package com.example.kate.gotosite;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText siteAddr;
    TextView tvError;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        siteAddr = (EditText) findViewById(R.id.siteAddr);
        tvError = (TextView) findViewById(R.id.tvEror);
    }

    public void onClick(View v) {
        try {
            String text = siteAddr.getText().toString();
            Uri uri = Uri.parse(text);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
            tvError.setVisibility(View.GONE);
        }catch (Exception ex)
        {
            tvError.setText(ex.toString());
            tvError.setVisibility(View.VISIBLE);
        }
    }
}
